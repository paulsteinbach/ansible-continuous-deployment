# Roles by Category

Application Servers

* vc_nifi
* vc_rialto
* vc_zookeeper
* vc_kafka
* vc_openjdk

Backup

Configuration

* vc_downloads

Voltron / Standalone

* vc_standalone
* vc_standalone_postinstall

Databases

* vc_cassandra
* vc_elasticsearch
* vc_kibana

Filesystems

* vc_nfs
* vc_samba

Kernel

* vc_selinux
* vc_sysctl

Logging

* vc_logrotate

Monitoring

* vc_elasticsearch_apm

Networking

Security

* vc_denyhosts
* vc_firewall
* vc_sshd
* vc_sudo

System Configuration

* vc_grub
* vc_ntpd
* vc_swapfile
* vc_system_groups
* vc_system_users
* vc_timezone
* vc_yum_defaults

Web Services

* vc_nginx

Naming Conventions

fix_ -- 
chk_
util_ 
